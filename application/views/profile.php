<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
	<head>
		<title>BloodShare</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css" media="screen" />
		<script src="<?php echo base_url();?>assets/bootstrap/js/jquery-1.11.2.min.js"></script>  
		<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/chart/Chart.js"></script>
	</head>
	<body>
		<div id="main_wrapper"class="container-fluid">
			<div class="row">
				<div id="header" class="col-md-12">
					<div id="logo" class="col-md-3">
						<h1><b>Blood</b>Share</h1>
					</div>
					<div id="nav" class="col-md-9">
						<nav class="navbar">
							<div class="container-fluid">							
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="#">Cari</a></li>
										<li class="dropdown">
										  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, Username <span class="caret"></span></a>
										  <ul class="dropdown-menu" role="menu">
											<li><a href="#">Edit Profil</a></li>		
											<li><a href="#">Logout</a></li>				
										  </ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div><!-- /.container-fluid -->
						</nav>		
					</div>
				</div>
			</div>
			<div id="content" class="row">
				<div id="content_left" class="col-md-2">
					<div id="menu">					
                        <ul class="nav nav-stacked">
						    <li role="presentation"><a href="#">Dasbor</a></li>
                            <li role="presentation"><a href="#">Semua Stok</a></li>
                        </ul> 
					</div>
				</div>
				<div id="content_right" class="col-md-10">
					<div class="container-fluid">
						<div id="page" class="row">
							<div id="page_title" class="col-md-4">
								<h3>Edit Profil</h3>
							</div>
							
							
							<div id="page_content" class="col-md-12">
								<form class="form-horizontal" action="#" method="POST">
									<div class="form-group">
										<label for="blood_type" class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" name="nama" id="nama" required />
										</div>
									</div>
									<div class="form-group">
										<label for="trans_type" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-5">
											<input type="email" class="form-control" name="email" id="email" required />
										</div>
									</div>
									<div class="form-group">
										<label for="amount" class="col-sm-2 control-label">No Telp</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" name="telp" id="telp" required />
										</div>         
									</div>	
									<div class="form-group">
										<label for="amount" class="col-sm-2 control-label">Alamat</label>
										<div class="col-sm-5">
											<textarea class="form-control" name="alamat" id="alamat" required ></textarea>
										</div>         
									</div>
									<div class="form-group">
										<label for="amount" class="col-sm-2 control-label">Provinsi</label>
										<div class="col-sm-5">
											<select class="form-control" name="provinsi" id="provinsi" required>
												<option value="0">Pilih Provinsi</option>
												<option value="1">Jawa Tengah</option>
											</select>
										</div>         
									</div>	
									<div class="form-group">
										<label for="amount" class="col-sm-2 control-label">Kota</label>
										<div class="col-sm-5">
											<select class="form-control" name="kota" id="kota" required>
												<option value="0">Pilih Kota</option>
												<option value="1">Semarang</option>
											</select>
										</div>         
									</div>
									<div class="form-group">
										<label for="image" class="col-sm-2 control-label">Upload Foto</label>
										<div class="col-sm-5">
											<div class="col-sm-9"id="input_file"><input id="upload_foto" placeholder="No file choosen" class="form-control" readonly="disabled" /></div>
											<div class="file_upload btn btn-primary">
												<span>Upload</span>
												<input type="file" class="upload" name="image" id="image" accept="image/*" required/>
											</div>
										</div>
									</div>									
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-primary" value="Simpan">
											<button type="button" class="btn btn-primary">Batal</button></a>
										</div>
									</div>
								</form>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="footer" class="col-md-12">
					&copy;Copyright Coconut_dev 2015
				</div>
			</div>
		</div>
					
	</body>
	<script>
		 $('#image').on('change', function(){
                var file = $(this)[0].files[0];
                if(file != undefined){
                    $('#upload_foto').val(file.name);
                }else{
                    $('#upload_foto').val('');
                }
            });
	</script>
	

</html>