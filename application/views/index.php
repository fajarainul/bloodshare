<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
	<head>
		<title>BloodShare</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css" media="screen" />
		<script src="<?php echo base_url();?>assets/bootstrap/js/jquery-1.11.2.min.js"></script>  
		<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/chart/Chart.js"></script>
	</head>
	<body>
		<div id="main_wrapper"class="container-fluid">
			<div class="row">
				<div id="header" class="col-md-12">
					<div id="logo" class="col-md-3">
						<h1><b>Blood</b>Share</h1>
					</div>
					<div id="nav" class="col-md-9">
						<nav class="navbar">
							<div class="container-fluid">							
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="#">Cari</a></li>
										<li class="dropdown">
										  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, Username <span class="caret"></span></a>
										  <ul class="dropdown-menu" role="menu">
											<li><a href="#">Edit Profil</a></li>
											<li><a href="#">Logout</a></li>				
										  </ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div><!-- /.container-fluid -->
						</nav>		
					</div>
				</div>
			</div>
			<div id="content" class="row">
				<div id="content_left" class="col-md-2">
					<div id="menu">					
                        <ul class="nav nav-stacked">
						    <li role="presentation"><a href="#">Dasbor</a></li>
                            <li role="presentation"><a href="#">Semua Stok</a></li>
                        </ul> 
					</div>
				</div>
				<div id="content_right" class="col-md-10">
					<div class="container-fluid">
						<div id="page" class="row">
							<div id="dashboard_stock" class="col-md-6">
								<div class="title">
									<h4>
										Jumlah Stok Sekarang
									</h4>
								</div>
								<div id="chart">
									<canvas id="myChart" width="400" height="200"></canvas>
								</div>
								<div id="button">
									<button class="btn btn-primary">Tambah Stok</button>
								</div>
							</div>
							<div id="dashboard_search" class="col-md-5">
								<div class="title">
									<h4>
										Pencarian
									</h4>
								</div>
								<div id="search">
									<p>
										Butuh Darah? Lakukan Pencarian
									</p>
									<form method="post" action="#">
										<div class="form-group">
											<label for="blood_type">Golongan Darah</label>
											<select class="form-control" id="blood_type" name="blood_type">
												<option value="1">A+</option>
												<option value="2">A-</option>
												<option value="3">B+</option>
												<option value="4">B-</option>
												<option value="5">AB+</option>
												<option value="6">AB-</option>
												<option value="7">0+</option>
												<option value="8">0-</option>
											</select>
										</div>
										<div class="form-group">
											<label for="trans_type">Jenis Transfusi</label>
											<select class="form-control" id="trans_type" name="trans_type">
												<option value="1">Darah Lengkap</option>
												<option value="2">Sel Darah Merah</option>
												<option value="3">Sel Darah Putih</option>
												<option value="4">Trombosit</option>
												<option value="5">Plasma</option>
												<option value="6">Fraksi Plasma</option>
											</select>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-primary" value="Cari"/>
										</div>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="footer" class="col-md-12">
					&copy;Copyright Coconut_dev 2015
				</div>
			</div>
		</div>
		<script>
			var ctx = document.getElementById("myChart").getContext("2d");
			var options = {
				scaleGridLineWidth : 1,
				barStrokeWidth : 2,
				barValueSpacing : 5,
				barDatasetSpacing : 1,
			}
			var data = {
				labels: ["A+", "A-", "B+", "B-", "AB+", "AB-", "0+","0-"],
				datasets: [
					{
						label: "My First dataset",
						fillColor: "rgba(220,220,220,0.5)",
						strokeColor: "rgba(220,220,220,0.8)",
						highlightFill: "rgba(220,220,220,0.75)",
						highlightStroke: "rgba(220,220,220,1)",
						data: [65, 59, 80, 81, 56, 55, 40,20]
					}
				]
			};
			var myBarChart = new Chart(ctx).Bar(data,options);						
		</script>			
	</body>
	

</html>