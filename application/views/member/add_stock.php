							<div id="page_title">
								<h3>Tambah Stok Darah</h3>
							</div>
							<div id="page_content" class="col-md-12">
								<form class="form-horizontal" action="#" method="POST">
									<div class="form-group">
										<label for="blood_type" class="col-sm-2 control-label">Golongan Darah</label>
										<div class="col-sm-5">
											<select class="form-control" id="blood_type" name="blood_type">
												<option value="1">A+</option>
												<option value="2">A-</option>
												<option value="3">B+</option>
												<option value="4">B-</option>
												<option value="5">AB+</option>
												<option value="6">AB-</option>
												<option value="7">0+</option>
												<option value="8">0-</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="trans_type" class="col-sm-2 control-label">Jenis Transfusi</label>
										<div class="col-sm-5">
											<select class="form-control" id="trans_type" name="trans_type">
												<option value="1">Darah Lengkap</option>
												<option value="2">Sel Darah Merah</option>
												<option value="3">Sel Darah Putih</option>
												<option value="4">Trombosit</option>
												<option value="5">Plasma</option>
												<option value="6">Fraksi Plasma</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="amount" class="col-sm-2 control-label">Jumlah</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" name="amount" id="amount" required />
										</div>  
										<?php echo form_error('amount'); ?>
									</div>				
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<input type="submit" class="btn btn-primary" value="Simpan">
											<button type="button" class="btn btn-primary">Batal</button></a>
										</div>
									</div>
								</form>	
							</div>