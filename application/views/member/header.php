<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
	<head>
		<title>BloodShare</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css" media="screen" />
		<script src="<?php echo base_url();?>assets/bootstrap/js/jquery-1.11.2.min.js"></script>  
		<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/chart/Chart.js"></script>
	</head>
	<body>
		<div id="main_wrapper"class="container-fluid">
			<div class="row">
				<div id="header" class="col-md-12">
					<div id="logo" class="col-md-3">
						<h1><b>Blood</b>Share</h1>
					</div>
					<div id="nav" class="col-md-9">
						<nav class="navbar">
							<div class="container-fluid">							
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="#">Cari</a></li>
										<li class="dropdown">
										  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, Username <span class="caret"></span></a>
										  <ul class="dropdown-menu" role="menu">
											<li><a href="#">Edit Profil</a></li>
											<li><a href="#">Logout</a></li>				
										  </ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div><!-- /.container-fluid -->
						</nav>		
					</div>
				</div>
			</div>
			<div id="content" class="row">
				<div id="content_left" class="col-md-2">
					<div id="menu">					
                        <ul class="nav nav-stacked">
						    <li role="presentation"><a href="#">Dasbor</a></li>
                            <li role="presentation"><a href="<?php echo site_url('member/all_stock');?>">Semua Stok</a></li>
                        </ul> 
					</div>
				</div>
				<div id="content_right" class="col-md-10">
					<div class="container-fluid">
						<div id="page" class="row">