							<div id="dashboard_stock" class="col-md-6">
								<div class="title">
									<h4>
										Jumlah Stok Sekarang
									</h4>
								</div>
								<div id="chart">
									<canvas id="myChart" width="400" height="200"></canvas>
								</div>
								<div id="button">
									<button class="btn btn-primary">Tambah Stok</button>
								</div>
							</div>
							<?php
								//untuk grafik
								$stok_darah = array(0,0,0,0,0,0,0,0);
								foreach($stok_golongan as $key=>$golongan){
									$stok_darah[$golongan['id_golongan']-1] = $golongan['jumlah'];			
								}
								
							?>
							<script>
								var ctx = document.getElementById("myChart").getContext("2d");
								var options = {
									scaleGridLineWidth : 1,
									barStrokeWidth : 2,
									barValueSpacing : 5,
									barDatasetSpacing : 1,
								}
								var data = {
									labels: ["A+", "A-", "B+", "B-", "AB+", "AB-", "0+","0-"],
									datasets: [
										{
											label: "My First dataset",
											fillColor: "rgba(220,220,220,0.5)",
											strokeColor: "rgba(220,220,220,0.8)",
											highlightFill: "rgba(220,220,220,0.75)",
											highlightStroke: "rgba(220,220,220,1)",
											data: [<?php foreach($stok_darah as $darah){
												echo $darah.',';
											}?>]
										}
									]
								};
								var myBarChart = new Chart(ctx).Bar(data,options);						
							</script>	
							<div id="dashboard_search" class="col-md-5">
								<div class="title">
									<h4>
										Pencarian
									</h4>
								</div>
								<div id="search">
									<p>
										Butuh Darah? Lakukan Pencarian
									</p>
									<form method="post" action="#">
										<div class="form-group">
											<label for="blood_type">Golongan Darah</label>
											<select class="form-control" id="blood_type" name="blood_type">
												<option value="1">A+</option>
												<option value="2">A-</option>
												<option value="3">B+</option>
												<option value="4">B-</option>
												<option value="5">AB+</option>
												<option value="6">AB-</option>
												<option value="7">0+</option>
												<option value="8">0-</option>
											</select>
										</div>
										<div class="form-group">
											<label for="trans_type">Jenis Transfusi</label>
											<select class="form-control" id="trans_type" name="trans_type">
												<option value="1">Darah Lengkap</option>
												<option value="2">Sel Darah Merah</option>
												<option value="3">Sel Darah Putih</option>
												<option value="4">Trombosit</option>
												<option value="5">Plasma</option>
												<option value="6">Fraksi Plasma</option>
											</select>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-primary" value="Cari"/>
										</div>	
									</form>
								</div>
							</div>