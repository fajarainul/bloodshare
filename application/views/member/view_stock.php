							<div id="page_title" class="col-md-4">
								<h3>Semua Stok Darah</h3>
							</div>
							<div id="add_button" class="col-md-4">
								<button class="btn btn-primary" onClick="location.href='<?php echo site_url('member/add_stock');?>'" role="button">Tambah Stok</button>
							</div>
							<div id="filter" class="col-md-12">
								<form class="form-inline">
									<div class="form-group">
										<select class="form-control" id="blood_type" name="blood_type">
											<option value="0" selected>Golongan Darah</option>
											<option value="1">A+</option>
											<option value="2">A-</option>
											<option value="3">B+</option>
											<option value="4">B-</option>
											<option value="5">AB+</option>
											<option value="6">AB-</option>
											<option value="7">0+</option>
											<option value="8">0-</option>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control" id="trans_type" name="trans_type">
											<option value="0" selected>Jenis Transfusi</option>
											<option value="1">Darah Lengkap</option>
											<option value="2">Sel Darah Merah</option>
											<option value="3">Sel Darah Putih</option>
											<option value="4">Trombosit</option>
											<option value="5">Plasma</option>
											<option value="6">Fraksi Plasma</option>
										</select>
									</div>
									<button type="submit" class="btn btn-primary">Filter</button>
								</form>
							</div>
							<div id="page_content" class="col-md-12">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Golongan Darah</th>
											<th>Jenis Transfusi</th>
											<th>Keterangan</th>
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($stok_darah as $stok){
										echo '<tr>';
										echo '<td><div class="item">'.$stok['nama_golongan'].'</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>';
										echo '<td>'.$stok['nama_transfusi'].'</td>';
										echo '<td>'.$stok['nama_tipe'].'</td>';
										echo '<td>'.$stok['jumlah'].' Kantong</td>';
									}
									
									?>
									</tbody>
								</table>
							</div>