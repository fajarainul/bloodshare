<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
	<head>
		<title>BloodShare</title>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css" media="screen" />
		<script src="<?php echo base_url();?>assets/bootstrap/js/jquery-1.11.2.min.js"></script>  
		<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/chart/Chart.js"></script>
	</head>
	<body>
		<div id="main_wrapper"class="container-fluid">
			<div class="row">
				<div id="header" class="col-md-12">
					<div id="logo" class="col-md-3">
						<h1><b>Blood</b>Share</h1>
					</div>
					<div id="nav" class="col-md-9">
						<nav class="navbar">
							<div class="container-fluid">							
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="#">Cari</a></li>
										<li class="dropdown">
										  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Hi, Username <span class="caret"></span></a>
										  <ul class="dropdown-menu" role="menu">
											<li><a href="#">Edit Profil</a></li>
											<li><a href="#">Logout</a></li>				
										  </ul>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div><!-- /.container-fluid -->
						</nav>		
					</div>
				</div>
			</div>
			<div id="content" class="row">
				<div id="content_left" class="col-md-2">
					<div id="menu">					
                        <ul class="nav nav-stacked">
						    <li role="presentation"><a href="#">Dasbor</a></li>
                            <li role="presentation"><a href="#">Semua Stok</a></li>
                        </ul> 
					</div>
				</div>
				<div id="content_right" class="col-md-10">
					<div class="container-fluid">
						<div id="page" class="row">
							<div id="page_title" class="col-md-4">
								<h3>Semua Stok Darah</h3>
							</div>
							<div id="add_button" class="col-md-4">
								<button class="btn btn-primary">Tambah Stok</button>
							</div>
							<div id="filter" class="col-md-12">
								<form class="form-inline">
									<div class="form-group">
										<select class="form-control" id="blood_type" name="blood_type">
											<option value="0" selected>Golongan Darah</option>
											<option value="1">A+</option>
											<option value="2">A-</option>
											<option value="3">B+</option>
											<option value="4">B-</option>
											<option value="5">AB+</option>
											<option value="6">AB-</option>
											<option value="7">0+</option>
											<option value="8">0-</option>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control" id="trans_type" name="trans_type">
											<option value="0" selected>Jenis Transfusi</option>
											<option value="1">Darah Lengkap</option>
											<option value="2">Sel Darah Merah</option>
											<option value="3">Sel Darah Putih</option>
											<option value="4">Trombosit</option>
											<option value="5">Plasma</option>
											<option value="6">Fraksi Plasma</option>
										</select>
									</div>
									<button type="submit" class="btn btn-primary">Filter</button>
								</form>
							</div>
							<div id="page_content" class="col-md-12">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Golongan Darah</th>
											<th>Jenis Transfusi</th>
											<th>Keterangan</th>
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div class="item">A+</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Darah Lengkap</td>
											<td>Stored</td>
											<td>100 Kantong</td>
										</tr>
										<tr>
											<td><div class="item">AB+</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Sel Darah Merah</td>
											<td>Packed RBC</td>
											<td>100 Kantong</td>
										</tr>
										<tr>
											<td><div class="item">0+</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Darah Lengkap</td>
											<td>Packed RBC</td>
											<td>100 Kantong</td>
										</tr>
										<tr>
											<td><div class="item">B+</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Sel Darah Merah</td>
											<td>Leukocyte Poor</td>
											<td>100 Kantong</td>
										</tr>
										<tr>
											<td><div class="item">0-</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Darah Lengkap</td>
											<td>Fresh</td>
											<td>100 Kantong</td>
										</tr><tr>
											<td><div class="item">B-</div><span class="action"><a href="#">Edit</a> <a href="#">Hapus</a></span></td>
											<td>Sel Darah Putih</td>
											<td>-</td>
											<td>100 Kantong</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="footer" class="col-md-12">
					&copy;Copyright Coconut_dev 2015
				</div>
			</div>
		</div>
					
	</body>
	

</html>