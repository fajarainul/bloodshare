<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Member extends CI_Model {
	public function get_blood_type($id=FALSE)
	{	
		$this->db->select('*');
		$this->db->from('stok_darah s');
		$this->db->join('darah d','s.id_darah = d.id_darah');
		$this->db->join('golongan g','d.id_golongan = g.id_golongan');
		$this->db->where('s.id_rs',1);	
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_stock($id=FALSE){
		$this->db->select('*');
		$this->db->from('stok_darah s');
		$this->db->join('darah d','s.id_darah = d.id_darah');
		$this->db->join('golongan g','d.id_golongan = g.id_golongan');
		$this->db->join('transfusi t','t.id_transfusi = d.id_transfusi');
		$this->db->join('tipe_transfusi tt','tt.id_tipe = t.id_tipe');
		$this->db->where('s.id_rs',1);	
		$query = $this->db->get();
		return $query->result_array();
	}
}