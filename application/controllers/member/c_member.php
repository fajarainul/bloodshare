<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Member extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Member');
	}
	public function index($id=false)
	{	
		/*
			keterangan:
			$id disini adalah id rumah sakit, 
			krena session belum dibuat, 
			maka default pada model M_Member class get_blood_type pake 1
		*/
		
		$data['stok_golongan'] = $this->M_Member->get_blood_type($id);
		$this->load->template('member/index',$data);

	}
	
	public function add_stock(){
		$this->load->helper('form');
		$this->load->library('form_validation');
		//set rules
		$this->form_validation->set_rules('amount', 'Jumlah Stok', 'trim|required|numeric');
		
		if ($this->form_validation->run() === FALSE)
		{
			$data['page']='form_artikel';
			$this->load->template('member/add_stock');
		}
		else
		{
			//$this->M_Member->add_stock();
			$this->load->template('member/index');
		}
		
	}
	
	public function all_stock($id=false){
		$data['stok_darah'] = $this->M_Member->get_stock($id);
		$this->load->template('member/view_stock',$data);
	}
	
	public function search(){
		$this->load->view('search');
	}
	
	public function profile(){
		$this->load->view('profile');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */